clc; clear ; close all;

sample_data =[rand(50,1)+1 , rand(50,1)+5];

plot(sample_data(:,1),sample_data(:,2),'k*','MarkerSize',5);

rng(1); % For reproducibility
[idx,C] = kmeans(sample_data,2);

x1 = min(sample_data(:,1)):0.01:max(sample_data(:,1));
x2 = min(sample_data(:,2)):0.01:max(sample_data(:,2));
[x1G,x2G] = meshgrid(x1,x2);
XGrid = [x1G(:),x2G(:)]; % Defines a fine grid on the plot

idx2Region = kmeans(XGrid,2,'MaxIter',1,'Start',C);

figure;
gscatter(XGrid(:,1),XGrid(:,2),idx2Region,...
    [0,0.75,0.75;0.75,0,0.75;0.75,0.75,0],'..');
hold on;
plot(sample_data(:,1),sample_data(:,2),'k*','MarkerSize',5);
title 'Fisher''s Iris Data';
xlabel 'Petal Lengths (cm)';
ylabel 'Petal Widths (cm)'; 
legend('Region 1','Region 2','Data','Location','SouthEast');
hold off;

opts = statset('Display','final');
[idx,C] = kmeans(sample_data,2,'Distance','cityblock',...
    'Replicates',5,'Options',opts);

figure;
plot(sample_data(idx==1,1),sample_data(idx==1,2),'r.','MarkerSize',12)
hold on
plot(sample_data(idx==2,1),sample_data(idx==2,2),'b.','MarkerSize',12)
plot(C(:,1),C(:,2),'kx',...
     'MarkerSize',15,'LineWidth',3) 
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')
title 'Cluster Assignments and Centroids'
hold off