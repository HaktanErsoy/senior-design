clc ; clear; close all;


 %% TEST PLOTTING OF NOISE ADDED IMAGES
close;

% subplot(351);
% imshow(imread('./NewWritten/1_1_1.pgm'));
% subplot(352);
% imshow(imread('./NewWritten/1_1_2.pgm'));
% subplot(353);
% imshow(imread('./NewWritten/1_1_3.pgm'));
% title('NOISE ADDED IMAGES')
% subplot(354);
% imshow(imread('./NewWritten/1_1_4.pgm'));
% subplot(355);
% imshow(imread('./NewWritten/1_1_5.pgm'));
% subplot(358);
% imshow(imread('./NormalizedTraficSigns/1_1.pgm'));
% title('ORIGINAL IMAGE')
% subplot(3,5,13);
% imshow(cell2mat(centerCell(:,:,1)));
% title('CLUSTER')

%% IMAGE WRITING AND CLUSTER CALCULATIONS
myDir = uigetdir; %gets directory
myFiles = dir(fullfile(myDir,'*.pgm')); %gets all txt files in struct

classes = [];

for k = 1:length(myFiles)
    baseFileName = myFiles(k).name;
    fullFileName = fullfile(myDir, baseFileName);
    gray =imread(fullFileName);    
    gray = imresize(gray,[70 70]);

    noise= 50;
    fileToWrite = './NewWritten70-50/';
    
    gray1= gray + uint8(normrnd(0,noise,70,70));
    gray2= gray + uint8(normrnd(0,noise,70,70));
    gray3= gray + uint8(normrnd(0,noise,70,70));
    gray4= gray + uint8(normrnd(0,noise,70,70));
    gray5= gray + uint8(normrnd(0,noise,70,70));
    
    for cs = 1:5
        if cs == 1
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray1,pathFile);
        elseif cs ==2
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray2,pathFile);
        elseif cs ==3
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray3,pathFile);
        elseif cs==4
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray4,pathFile);
        else
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray5,pathFile);
        end       
    end
    
    
end
%% 
clc; clear;
fileToWrite = './NewWritten70-25/';
newFiles = dir(fullfile(fileToWrite,'*.pgm'));

myDir = uigetdir; %gets directory
myFiles = dir(fullfile(myDir,'*.pgm')); %gets all txt files in struct


summation = 0;
average = [];
imageMat=[];
centerMat = [];
for k = 1:length(myFiles)    
    
   summation = 0;  
   for m = 1:5
    newBaseName = newFiles((5*(k-1))+m).name;
    newFullName = strcat(fileToWrite,newBaseName);
    img =imread(newFullName);
    meanImg = mean(img(:));
    bin = img > meanImg;
    summation = summation + bin;
    imagesCell(:,:,k) = mat2cell(bin, [7 7 7 7 7 7 7 7 7 7],[7 7 7 7 7 7 7 7 7 7]); % IMAGES

    
        
    if m == 5
        for i = 1:10
            for j = 1:10
                if(sum(sum(cell2mat(imagesCell(i,j,k))))>=25)
                   imageMat(i,j) = 1;
                else
                    imageMat(i,j) = 0;
                end
            end
            end
        end
        
    end    
   end




colnum =repmat(70,length(imagesCell),1)';
middleman= mat2cell(average,[70],colnum); 

for k=1:length(imagesCell)
    centerCell(:,:,k) = mat2cell(cell2mat(middleman(k)),[7 7 7 7 7 7 7 7 7 7],[7 7 7 7 7 7 7 7 7 7]); % CLUSTER CENTERS
end

for k = 1:length(myFiles)
for i = 1:10
    for j = 1:10
        if(sum(sum(cell2mat(centerCell(i,j,k))))>=25)
           centerMat(i,j) = 1;
        else
            centerMat(i,j) = 0;
        end
    end
    end
end




clc;
feature = 0;
ctr = 0;
ctxarr = zeros(1,length(imagesCell));
for total = 1:10
    for trial = 1:100
        seed=randi(length(imagesCell));    
        for m = 1:length(imagesCell)
            for i = 1:10
                for j = 1:10
                    asil = cell2mat(imagesCell(i,j,seed));
                    kiyas = cell2mat(centerCell(i,j,m));
                    if sum(sum(abs(kiyas-asil))) <= 0
                        feature = feature +1;
                    end                
                end            
            end 
            ctxarr(m) = feature;
            feature = 0;
        end       
        
        [maxVal,indexVal] = max(ctxarr);
%         subplot(121);
%         imshow(cell2mat(imagesCell(:,:,seed)));
%         title('ACTUAL IMAGE');
%         subplot(122);
%         imshow(cell2mat(centerCell(:,:,indexVal)));
%         title('PREDICTED IMAGE');
%         pause(0.5);
        
        if seed == indexVal
           ctr = ctr + 1; 
        end
        if trial == 100
           fprintf('The accuracy for trial %d: %d  \n',total,ctr) 
        end
    end
    ctr = 0;
end

%%

% clear; close;
% actImages = dir(fullfile('./NormalizedTraficSigns','*.pgm'));
% for i = 1:length(actImages)
%     actBase = actImages(i).name;
%     actFull = strcat('./NormalizedTraficSigns/',actBase);
%     img =imread(actFull);
%     
%     imshow(img);
%     pause(1);     
%     fprintf('%d \n',i);
%     
% end
% 

%%
clear; clc;

a= randi(255,30,30);
b = imnoise(a,'gaussian',0,0.1);
A = rescale(a);


Aim=mat2gray(A);
bim = mat2gray(b);

subplot(211);
imshow(Aim);
subplot(212);
imshow(mat2gray(a));

%%











