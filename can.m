clc ; clear; close all;

%%
myDir = uigetdir; %gets directory
myFiles = dir(fullfile(myDir,'*.pgm')); %gets all txt files in struct

classes = [];
summation = 0;
average = [];
for k = 1:length(myFiles)
    baseFileName = myFiles(k).name;
    fullFileName = fullfile(myDir, baseFileName);
    gray =imread(fullFileName);    
    gray = imresize(gray,[30 30]);

    gray1= imnoise(gray,'gaussian',0,0.02);
    gray2= imnoise(gray,'gaussian',0,0.02);
    gray3= imnoise(gray,'gaussian',0,0.02);
    gray4= imnoise(gray,'gaussian',0,0.02);
    gray5= imnoise(gray,'gaussian',0,0.02);

    gray =imread(fullFileName);
    gray = imresize(gray,[30 30]);  
    meanInt = mean(gray(:));
    bin = gray > meanInt;
    imagesCell(:,:,k) = mat2cell(bin, [5 5 5 5 5 5],[5 5 5 5 5 5]); % IMAGES

  
    for cs = 1:5
        if cs == 1
            pathFile = strcat('./NewWritten/',replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray1,pathFile);
        elseif cs ==2
            pathFile = strcat('./NewWritten/',replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');        
            imwrite(gray2,pathFile);
        elseif cs ==3

            pathFile = strcat('./NewWritten/',replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');        
            imwrite(gray3,pathFile);
        elseif cs==4

            pathFile = strcat('./NewWritten/',replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');        
            imwrite(gray4,pathFile);
        else
            pathFile = strcat('./NewWritten/',replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray5,pathFile);
        end       
    end
    
    
    newFiles = dir(fullfile('./NewWritten','*.pgm'));
    
   summation = 0;  
   for m = 1:5
    newBaseName = newFiles((5*(k-1))+m).name;
    newFullName = strcat('./NewWritten/',newBaseName);
    img =imread(newFullName);
    meanImg = mean(img(:));
    bin = img > meanImg;
    summation = summation + bin;
        
    if m == 5
        avg = summation / 5.0;
        for i = 1:900
            if avg(i) < 0.5
                avg(i) = 0;
            else
                avg(i) = 1;
            end    
        end
        average = [average, avg];
    end    
   end
   
   
   
end

colnum =repmat(30,length(imagesCell),1)';
middleman= mat2cell(average,[30],colnum); 

for k=1:length(imagesCell)
    centerCell(:,:,k) = mat2cell(cell2mat(middleman(k)),[5 5 5 5 5 5],[5 5 5 5 5 5]); % CLUSTER CENTERS
end


 %% TEST PLOTTING OF NOISE ADDED IMAGES
%      
% subplot(151);
% imshow(imread('./NewWritten/5_a_1.pgm'));
% subplot(152);
% imshow(imread('./NewWritten/5_a_2.pgm'));
% subplot(153);
% imshow(imread('./NewWritten/5_a_3.pgm'));
% subplot(154);
% imshow(imread('./NewWritten/5_a_4.pgm'));
% subplot(155);
% imshow(imread('./NewWritten/5_a_5.pgm'));

%%

clc;
feature = 0;
ctr = 0;
ctxarr = zeros(1,length(imagesCell));
for total = 1:10
    for trial = 1:100
        seed=randi(length(imagesCell));    
        for m = 1:length(imagesCell)
            for i = 1:6
                for j = 1:6
                    asil = cell2mat(imagesCell(i,j,seed));
                    kiyas = cell2mat(centerCell(i,j,m));
                    if sum(sum(abs(kiyas-asil))) <= 0
                        feature = feature +1;
                    end                
                end            
            end 
            ctxarr(m) = feature;
            feature = 0;
        end       
        
        [maxVal,indexVal] = max(ctxarr);
%         subplot(121);
%         imshow(cell2mat(imagesCell(:,:,seed)));
%         title('actual image');
%         subplot(122);
%         imshow(cell2mat(centerCell(:,:,indexVal)));
%         title('predicted image');
%         pause(1);
        
        if seed == indexVal
           ctr = ctr + 1; 
        end
        if trial == 100
           fprintf('The accuracy for trial %d: %d  \n',total,ctr) 
        end
    end
    ctr = 0;
end

%%

% clear; close;
% actImages = dir(fullfile('./NormalizedTraficSigns','*.pgm'));
% for i = 1:length(actImages)
%     actBase = actImages(i).name;
%     actFull = strcat('./NormalizedTraficSigns/',actBase);
%     img =imread(actFull);
%     
%     imshow(img);
%     pause(1);
%     
%     
%     
%     fprintf('%d \n',i);
%     
% end
% 










