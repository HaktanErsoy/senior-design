function [value, kindex] = knn(img,imgArray,K,type)


mesafe = [];

if type == "feature"
    for k=1:length(imgArray)
        d=0;
        for i=1:10
            for j=1:10
                d = d+(img(i,j)-imgArray(i,j,k))^2;
            end
        end
        mesafe(k)=sqrt(d);
    end
    
    
elseif type=="im"
    img=im2double(img).*255;
    imgArray=im2double(imgArray).*255;
    for k=1:length(imgArray)
        d=0;
        for i=1:70
            for j=1:70
                d = d+(img(i,j)-imgArray(i,j,k))^2;
            end
        end
    mesafe(k)=sqrt(d);
    end 
    
    
end




[value,index] = sort(mesafe);
kindex(1:K)=index(1:K);
end

