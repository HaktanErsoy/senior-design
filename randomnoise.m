clc ; clear; close all;


 %% TEST PLOTTING OF NOISE ADDED IMAGES
% close;
% 
% subplot(351);
% imshow(imread('./NewWritten70-07-02/1_2_1.pgm'));
% subplot(352);
% imshow(imread('./NewWritten70-07-02/1_2_2.pgm'));
% subplot(353);
% imshow(imread('./NewWritten70-07-02/1_2_3.pgm'));
% title('NOISE ADDED IMAGES')
% subplot(354);
% imshow(imread('./NewWritten70-07-02/1_2_4.pgm'));
% subplot(355);
% imshow(imread('./NewWritten70-07-02/1_2_5.pgm'));
% subplot(358);
% imshow(imread('./NormalizedTraficSigns/1_2.pgm'));
% title('ORIGINAL IMAGE')
% subplot(3,5,13);
% imshow(cell2mat(centerCell(:,:,2)));
% title('CLUSTER')

%% IMAGE WRITING AND CLUSTER CALCULATIONS

dirname = './NormalizedTraficSigns';
myFiles = dir(fullfile(dirname,'*.pgm')); %gets all txt files in struct



for k = 1:length(myFiles)
    baseFileName = myFiles(k).name;
    fullFileName = fullfile(dirname, baseFileName);
    gray =imread(fullFileName);    
    gray = imresize(gray,[70 70]);
    meanImg = mean(gray(:));
    bin = gray > meanImg;
    
    imagesCell(:,:,k) = mat2cell(gray, repmat(7,[1 10]),repmat(7,[1 10])); % IMAGES
    binImagesCell(:,:,k) = mat2cell(bin, repmat(7,[1 10]),repmat(7,[1 10]));

    noise_var= 0.2;
    noise_mean = 0.5;
    fileToWrite = './imageFiles/NewWritten70-05-02/';
    
    gray1= imnoise(gray,'gaussian',noise_mean,noise_var);
    gray2= imnoise(gray,'gaussian',noise_mean,noise_var);
    gray3= imnoise(gray,'gaussian',noise_mean,noise_var);
    gray4= imnoise(gray,'gaussian',noise_mean,noise_var);
    gray5= imnoise(gray,'gaussian',noise_mean,noise_var);
    
    for i = 1:5
        if i==1
            meanImg = mean(gray1(:));
            bin = gray > meanImg;
            noisyImagesCell(:,:,5*(k-1)+i) = mat2cell(gray1, repmat(7,[1 10]),repmat(7,[1 10]));
            noisyBinImagesCell(:,:,5*(k-1)+i) = mat2cell(bin, repmat(7,[1 10]),repmat(7,[1 10]));
        elseif i == 2
            meanImg = mean(gray2(:));
            bin = gray > meanImg;
            noisyImagesCell(:,:,5*(k-1)+i) = mat2cell(gray2, repmat(7,[1 10]),repmat(7,[1 10]));
            noisyBinImagesCell(:,:,5*(k-1)+i) = mat2cell(bin, repmat(7,[1 10]),repmat(7,[1 10]));
        elseif i==3
            meanImg = mean(gray3(:));
            bin = gray > meanImg;  
            noisyImagesCell(:,:,5*(k-1)+i) = mat2cell(gray3, repmat(7,[1 10]),repmat(7,[1 10]));
            noisyBinImagesCell(:,:,5*(k-1)+i) = mat2cell(bin, repmat(7,[1 10]),repmat(7,[1 10]));
        elseif i==4
            meanImg = mean(gray4(:));
            bin = gray > meanImg;
            noisyImagesCell(:,:,5*(k-1)+i) = mat2cell(gray4, repmat(7,[1 10]),repmat(7,[1 10]));
            noisyBinImagesCell(:,:,5*(k-1)+i) = mat2cell(bin, repmat(7,[1 10]),repmat(7,[1 10]));
        else
            meanImg = mean(gray5(:));
            bin = gray > meanImg;
            noisyImagesCell(:,:,5*(k-1)+i) = mat2cell(gray5, repmat(7,[1 10]),repmat(7,[1 10]));
            noisyBinImagesCell(:,:,5*(k-1)+i) = mat2cell(bin, repmat(7,[1 10]),repmat(7,[1 10]));

        end
    end
    
    for cs = 1:5
        if cs == 1
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray1,pathFile);
        elseif cs ==2
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray2,pathFile);
        elseif cs ==3
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray3,pathFile);
        elseif cs==4
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray4,pathFile);
        else
            pathFile = strcat(fileToWrite,replace(baseFileName,'.pgm',''),'_',string(cs),'.pgm');
            imwrite(gray5,pathFile);
        end       
    end
    
    
end
%% 
% clc; clear;

newFiles = dir(fullfile(fileToWrite,'*.pgm'));


dirname = './NormalizedTraficSigns';
myFiles = dir(fullfile(dirname,'*.pgm')); %gets all txt files in struct

summation = 0;
average = [];
featureMat=[];
centerMat = [];
for k = 1:length(myFiles)    
    
   summation = 0;
      
    % FOR ALL 320 NOISY IMAGES
    for l = 1:5
        for i = 1:10
            for j = 1:10
                if(sum(sum(cell2mat(noisyBinImagesCell(i,j,5*(k-1)+l))))>=30) 
                    noiseFeatureMat(i,j) = 1;
                else
                    noiseFeatureMat(i,j) = 0;
                end
                if(sum(sum(cell2mat(binImagesCell(i,j,k))))>=30) 
                    featureMat(i,j) = 1;
                else
                    featureMat(i,j) = 0;
                end
                    imWithFeatures(i,j,k)=featureMat(i,j);
                    allWithFeatures(i,j,5*(k-1)+l)=noiseFeatureMat(i,j);

            end
        end   
    end
    
    
   for m = 1:5
    newBaseName = newFiles((5*(k-1))+m).name;
    newFullName = strcat(fileToWrite,newBaseName);
    img =imread(newFullName);
    meanImg = mean(img(:));
    bin = img > meanImg;
    summation = summation + bin;   
   end
   
   for i=1:4900
       if summation(i)>=3
          summation(i)=1;
       else
          summation(i)=0;
       end
   end
   
   
   centerCell(:,:,k) = mat2cell(summation, repmat(7,[1 10]),repmat(7,[1 10]));
   
       for i = 1:10
            for j = 1:10
                if(sum(sum(cell2mat(centerCell(i,j,k))))>=30)
                   centerMat(i,j) = 1;
                else
                    centerMat(i,j) = 0;
                end
                    centerWithFeatures(i,j,k)=centerMat(i,j);

            end
       end   
   end

clc;
ctr = 0;
ctrMat = [];
ctxarr = zeros(1,length(binImagesCell));
for total = 1:100
    for trial = 1:100
        seed=randi(length(binImagesCell));    
        for m = 1:length(binImagesCell)
            benzeme = 0;
            for i = 1:10
                for j = 1:10
                    asil = imWithFeatures(i,j,m);
                    kiyas = centerWithFeatures(i,j,seed);
                    if asil == kiyas
                        benzeme = benzeme + 1;
                    end                
                end            
            end
            benzesmeMat(m) = benzeme;                       
        end               
        [maxVal,indexVal] = max(benzesmeMat);
%         subplot(121);
%         imshow((imWithFeatures(:,:,seed)));
%         title(['ACTUAL IMAGE',num2str(seed)]);
%         subplot(122);
%         imshow((centerWithFeatures(:,:,indexVal)));
%         title(['PREDICTED IMAGE',num2str(indexVal)]);
%         pause(0.5);
        
        if seed == indexVal
           ctr = ctr + 1; 
        end
        if trial == 100
           fprintf('The accuracy for trial %d: %d  \n',total,ctr) 
        end
    end
    ctrMat = [ctrMat ctr];
    ctr = 0;
    
end







%%

centerKNNFeatures=[];
imKNNFeatures=[];
ctrMat2 =[];

for i=1:320         
        middleman = allWithFeatures(:,:,i);
        middleman= reshape(middleman,[1,100]);  
        centerKNNFeatures = cat(1,centerKNNFeatures,middleman);      
end



for total = 1:100
    trial = 0;
    for trial = 1:100
        seed=randi(length(binImagesCell));
        middleman = imWithFeatures(:,:,seed);   
        imKNNFeatures = reshape(middleman,[1,100]);  
%       Idx = knnsearch(centerKNNFeatures(:,:),imKNNFeatures(:,:),'K',5);         
        [value,output] = knn(imWithFeatures,allWithFeatures(:,:,seed),5,"feature");
%         
%         subplot(121);
%         imshow(cell2mat(centerCell(:,:,seed)));
%         title(['ACTUAL IMAGE',num2str(seed)]);
%         subplot(122);
%         imshow(cell2mat(imagesCell(:,:,round(mean(output/5)))));
%         title(['PREDICTED IMAGE',num2str(round(mean(output/5)))]);
%         pause(0.5);
        
        if seed - round(mean(output/5)) == 0
           ctr = ctr + 1; 
        end
        if trial == 100
            fprintf('accuracy for trial %d is %d \n',total,ctr);
        end
    end
    ctrMat2 = [ctrMat2 ctr];
    ctr = 0;    
end
disp(mean(ctrMat2))
disp(mean(ctrMat))


%%

arr = [];

arr = tmatch(cell2mat(imagesCell(:,:,2)),cell2mat(imagesCell));

% abc = normxcorr2(cell2mat(centerCell(:,:,1)),cell2mat(imagesCell(:,:,1)));

%%

% clear; close;
% actImages = dir(fullfile('./NormalizedTraficSigns','*.pgm'));
% for i = 1:length(actImages)
%     actBase = actImages(i).name;
%     actFull = strcat('./NormalizedTraficSigns/',actBase);
%     img =imread(actFull);
%     
%     imshow(img);
%     pause(1);     
%     fprintf('%d \n',i);
%     
% end
% 
